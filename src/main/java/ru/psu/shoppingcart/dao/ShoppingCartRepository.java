package ru.psu.shoppingcart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.psu.shoppingcart.model.Guest;
import ru.psu.shoppingcart.model.ShoppingCart;

import java.util.UUID;

public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, UUID> {
}

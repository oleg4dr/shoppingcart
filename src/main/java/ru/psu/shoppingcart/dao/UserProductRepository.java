package ru.psu.shoppingcart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.psu.shoppingcart.model.Guest;
import ru.psu.shoppingcart.model.UserProduct;

import java.util.UUID;

public interface UserProductRepository extends JpaRepository<UserProduct, UUID> {
}

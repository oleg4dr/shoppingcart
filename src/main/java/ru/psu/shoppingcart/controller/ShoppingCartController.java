package ru.psu.shoppingcart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.psu.shoppingcart.model.ShoppingCart;
import ru.psu.shoppingcart.controller.dto.GuestDto;
import ru.psu.shoppingcart.service.ShoppingCartService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class ShoppingCartController {

    private ShoppingCartService shoppingCartService;

    @Autowired
    public ShoppingCartController(ShoppingCartService shoppingCartService) {
        this.shoppingCartService = shoppingCartService;
    }

    @GetMapping("/")
    public List<ShoppingCart> getAllShoppingCarts(){
        System.out.println("all carts");
        return shoppingCartService.findAll();
    }

    @GetMapping("/hello")
    public String printHello(){
       return  "HELLO!";
    }

    //списко товаров корзины
    @GetMapping("/shoppingCart/{cartId}")
    public Optional<ShoppingCart> getShopCartById(@PathVariable UUID cartId){
        return shoppingCartService.findByShoppingCartId(cartId);
    }

    @GetMapping("/shoppingCart")

    @PostMapping("/user/{userId}/catalog/")
    public String addProductInShopCart(@RequestBody GuestDto guest, @PathVariable UUID userId){
//        shoppingCartService.save(dto->Guest);
        return "Product added";
    }
//    @GetMapping("/user/id/{userId}/cart")
//    public ShoppingCartPojo getShoppingCart(@PathVariable UUID userId) {
//        return shoppingCartService.findByUserId(userId);
//    }

}

package ru.psu.shoppingcart.controller.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserProductDto {
    private UUID userProductId;

    private UUID userId;

    private UUID productId;

    private UUID guestId;

    private Collection<ShoppingCartDto> carts;
}

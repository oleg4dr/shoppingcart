package ru.psu.shoppingcart.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.psu.shoppingcart.model.CartStatusEnum;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ShoppingCartDto {
    private UUID shoppingCartId;

    private UUID userProduct;

    private CartStatusEnum status;

    private LocalDateTime dateTime;

    private List <UserProductDto> userProductsList = new ArrayList<>();

}

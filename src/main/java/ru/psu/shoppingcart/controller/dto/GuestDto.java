package ru.psu.shoppingcart.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GuestDto {
    private UUID guestId;

    private String mail;

    private Integer phoneNumber;

    private Collection<UserProductDto> userProducts;
}

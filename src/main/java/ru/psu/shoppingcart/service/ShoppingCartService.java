package ru.psu.shoppingcart.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.psu.shoppingcart.model.ShoppingCart;

import java.util.Optional;
import java.util.UUID;


@Service
public interface ShoppingCartService extends JpaRepository<ShoppingCart,UUID> {
    Optional<ShoppingCart> findByShoppingCartId(UUID cartId);
//    ShoppingCartPojo findByUserId(UUID userId);
}
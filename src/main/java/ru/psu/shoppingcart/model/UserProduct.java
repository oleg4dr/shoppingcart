package ru.psu.shoppingcart.model;


import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity
@Table(name = "user_product")
public class UserProduct {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "user_product_id", updatable = false, nullable = false)
    private UUID userProductId;

    @Column(name = "user_id")
    private UUID userId;

    @Column(name = "product_id")
    private UUID productId;

    @ManyToOne
    @JoinColumn(name = "guest_id")
//    private UUID guestId;
    private Guest guest;

//    @OneToMany(mappedBy = "userProduct")
//    private Collection<ShoppingCart> carts;
}

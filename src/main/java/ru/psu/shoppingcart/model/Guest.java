package ru.psu.shoppingcart.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity
@Table(name = "guest")
public class Guest {



    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "guest_id", updatable = false, nullable = false)
    private UUID guestId;

    @Column(name = "mail")
    private String mail;

    @Column(name = "phone_number")
    private Integer phoneNumber;

//    @OneToMany(mappedBy = "guest")
//    private Collection<UserProduct> userProducts;
}

package ru.psu.shoppingcart.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity
@Table(name = "shopping_cart")
public class ShoppingCart {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "shopping_cart_id", updatable = false, nullable = false)
    private UUID shoppingCartId;

    @ManyToOne(targetEntity = UserProduct.class)
    @JoinColumn(name = "user_product_id")
//    private UUID userProductId;
    private UserProduct userProduct;

    @Enumerated(EnumType.STRING)
    private CartStatusEnum status;

    @Column(name = "date")
    private LocalDateTime dateTime;

}

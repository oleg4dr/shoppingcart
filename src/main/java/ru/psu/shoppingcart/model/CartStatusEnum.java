package ru.psu.shoppingcart.model;

public enum CartStatusEnum {
    Completed, InProgress, Denied, Unidentified
}

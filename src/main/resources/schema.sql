DROP TABLE IF EXISTS shopping_cart, guest, user_product;

CREATE TABLE user_product(
                             id UUID PRIMARY KEY DEFAULT uuid_generate_v1(),
                             user_id UUID,
                             product_id UUID,
                             guest_id UUID,

--                              FOREIGN KEY (user_id) REFERENCES user(id),
--                              FOREIGN KEY (product_id) REFERENCES product(id),
                              FOREIGN KEY (guest_id) REFERENCES guest(id)
);

CREATE TABLE shopping_cart (
                               id UUID PRIMARY KEY DEFAULT uuid_generate_v1(),
                               user_product_id UUID,
                               status status_enum,
                               date DATE,

                               FOREIGN KEY (user_product_id) REFERENCES user_product(id)
);

CREATE TABLE guest(
                      id UUID PRIMARY KEY DEFAULT uuid_generate_v1(),
                      mail varchar(50),
                      phone_number integer
);

CREATE TYPE status_enum AS ENUM (
    'Completed',
    'InProgress',
    'Denied'
    );
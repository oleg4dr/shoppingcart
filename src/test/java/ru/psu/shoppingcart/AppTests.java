package ru.psu.shoppingcart;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.psu.shoppingcart.dao.GuestRepository;
import ru.psu.shoppingcart.dao.ShoppingCartRepository;
import ru.psu.shoppingcart.dao.UserProductRepository;
import ru.psu.shoppingcart.model.Guest;
import ru.psu.shoppingcart.model.ShoppingCart;
import ru.psu.shoppingcart.service.ShoppingCartService;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTests {

    @Autowired
    GuestRepository guestRepository;
    @Autowired
    ShoppingCartRepository shoppingCartRepository;
    @Autowired
    UserProductRepository userProductRepository;

//    @Test
//    public void contextLoads() {
//        Guest guest = new Guest();
//        guest.setPhoneNumber(1234567);
//        guest.setMail("bb@aa1s2.bb");
//
//        guest = guestRepository.save(guest);
//        System.out.println("Guest id: " + guest.getGuestId());
//        //проверить CRUD на все репозитории
//    }

//    @Test
//    public void shoppingCartTest(){
//        ShoppingCart cart = new ShoppingCart();
//        cart.setDateTime(LocalDateTime.now());
//        cart.setShoppingCartId();
//        cart.setUserProduct();
//        cart = shoppingCartRepository.save(cart);
//        System.out.println("Cart id: " + cart.getShoppingCartId());
//    }

}
